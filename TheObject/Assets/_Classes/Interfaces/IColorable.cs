﻿using UnityEngine;

public interface IColorable
{
	void ChangeColor(Color color);
	void RunPattern(Color[] colors, float duration);
	void ReturnToOriginal();
}
﻿using UnityEngine;

public interface IAudioChanger
{
	bool IsPlaying { get; }
	void ChangeAudioClip(AudioClip clip);
	void TogglePlaying();
}

﻿using UnityEngine;

public interface IRotateable
{
	void Rotate(Vector3 eulerAngles);
	void Rotate(Quaternion rotation);
}

﻿using UnityEngine;

public interface IActionable
{
	void Action(Vector3 movement, Vector3 eulerAngles);
}

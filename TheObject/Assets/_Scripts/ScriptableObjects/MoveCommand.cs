﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MoveCommand",
	menuName = "Commands/MoveCommand", order = 1)]
public class MoveCommand : ACommand
{
	[SerializeField]
	private Vector3 Movement;

	public override void Execute<OType>(OType target)
	{
		if (target is IMoveable moveable)
		{
			ExecuteMovement(moveable);
		}
		else
		{
			Debug.LogError($"The supplied behaviour {target.name} does not implement IMoveable.");
		}
	}

	private void ExecuteMovement(IMoveable moveable)
	{
		moveable.Move(Movement);
	}
}

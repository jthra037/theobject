﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ACommand : ScriptableObject
{
	public abstract void Execute<OType>(OType target) where OType : MonoBehaviour;
}

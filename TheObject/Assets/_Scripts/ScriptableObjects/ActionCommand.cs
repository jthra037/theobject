﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ActionCommand",
	menuName = "Commands/ActionCommand", order = 1)]
public class ActionCommand : ACommand
{
	[SerializeField]
	private Vector3 Movement;
	[SerializeField]
	private Vector3 Rotation;

	public override void Execute<OType>(OType target)
	{
		if (target is IActionable actionable)
		{
			ExecuteAction(actionable);
		}
		else
		{
			Debug.LogError($"The supplied behaviour {target.name} does not implement IMoveable.");
		}
	}

	private void ExecuteAction(IActionable actionable)
	{
		actionable.Action(Movement, Rotation);
	}
}

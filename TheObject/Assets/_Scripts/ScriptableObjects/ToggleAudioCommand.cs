﻿using UnityEngine;

[CreateAssetMenu(fileName = "ToggleAudioCommand",
	menuName = "Commands/ToggleAudioCommand", order = 1)]
public class ToggleAudioCommand : ACommand
{
	public override void Execute<OType>(OType target)
	{
		if (target is IAudioChanger audioChanger)
		{
			ChangeAudio(audioChanger);
		}
		else
		{
			Debug.LogError($"The supplied behaviour {target.name} does not implement IMoveable.");
		}
	}

	private void ChangeAudio(IAudioChanger audioChanger)
	{
		audioChanger.TogglePlaying();
	}
}
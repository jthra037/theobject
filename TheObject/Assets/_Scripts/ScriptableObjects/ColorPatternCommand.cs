﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ColorPatternCommand",
	menuName = "Commands/ColorPatternCommand", order = 1)]
public class ColorPatternCommand : ACommand
{
	[SerializeField]
	private Color[] colors;
	[SerializeField]
	private float duration;

	public override void Execute<OType>(OType target)
	{
		if (target is IColorable colorable)
		{
			ChangeColor(colorable);
		}
		else
		{
			Debug.LogError($"The supplied behaviour {target.name} does not implement IMoveable.");
		}
	}

	private void ChangeColor(IColorable colorable)
	{
		colorable.RunPattern(colors, duration);
	}
}

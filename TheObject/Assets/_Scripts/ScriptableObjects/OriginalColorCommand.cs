﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "OriginalColorCommand",
	menuName = "Commands/OriginalColorCommand", order = 1)]
public class OriginalColorCommand : ACommand
{
	public override void Execute<OType>(OType target)
	{
		if (target is IColorable colorable)
		{
			colorable.ReturnToOriginal();
		}
		else
		{
			Debug.LogError($"The supplied behaviour {target.name} does not implement IMoveable.");
		}
	}
}

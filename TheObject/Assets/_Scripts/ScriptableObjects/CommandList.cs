﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(fileName = "CommandList", menuName = "Configs/CommandList", order = 1)]
public class CommandList : ScriptableObject
{
	[System.Serializable]
	private struct KeyCommandPairing
	{
		public KeyCode Key;
		public ACommand Command;
	}

	[SerializeField]
	private KeyCommandPairing[] commands;

	public Dictionary<KeyCode, ACommand> GetCommandMap()
	{
		Dictionary<KeyCode, ACommand> map = new Dictionary<KeyCode, ACommand>();
		
		foreach(KeyCommandPairing commandPairing in commands)
		{
			map.Add(commandPairing.Key, commandPairing.Command);
		}

		return map;
	}
}

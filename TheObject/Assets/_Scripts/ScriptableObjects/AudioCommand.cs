﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AudioCommand",
	menuName = "Commands/AudioCommand", order = 1)]
public class AudioCommand : ACommand
{
	[SerializeField]
	private AudioClip clip;

	public override void Execute<OType>(OType target)
	{
		if (target is IAudioChanger audioChanger)
		{
			ChangeAudio(audioChanger);
		}
		else
		{
			Debug.LogError($"The supplied behaviour {target.name} does not implement IMoveable.");
		}
	}

	private void ChangeAudio(IAudioChanger audioChanger)
	{
		audioChanger.ChangeAudioClip(clip);
	}
}

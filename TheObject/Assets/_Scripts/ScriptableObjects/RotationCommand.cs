﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MoveCommand",
	menuName = "Commands/RotationCommand", order = 1)]
public class RotationCommand : ACommand
{
	[SerializeField]
	private Vector3 EulerAngles;

	public override void Execute<OType>(OType target)
	{
		if (target is IRotateable rotateable)
		{
			ExecuteRotation(rotateable);
		}
		else
		{
			Debug.LogError($"The supplied behaviour {target.name} does not implement IMoveable.");
		}
	}

	private void ExecuteRotation(IRotateable rotateable)
	{
		rotateable.Rotate(EulerAngles);
	}
}

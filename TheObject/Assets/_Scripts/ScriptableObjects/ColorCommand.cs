﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ColorCommand",
	menuName = "Commands/ColorCommand", order = 1)]
public class ColorCommand : ACommand
{
	[SerializeField]
	private Color color;

	public override void Execute<OType>(OType target)
	{
		if (target is IColorable colorable)
		{
			ChangeColor(colorable);
		}
		else
		{
			Debug.LogError($"The supplied behaviour {target.name} does not implement IMoveable.");
		}
	}

	private void ChangeColor(IColorable colorable)
	{
		colorable.ChangeColor(color);
	}
}

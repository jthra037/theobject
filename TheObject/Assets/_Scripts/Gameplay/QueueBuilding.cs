﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class QueueBuilding : MonoBehaviour
{
	[SerializeField]
	private TextMeshProUGUI buttonText;
	
	private ACommandHandler[] handlers;

	private bool isExecuteImmediately = true;

	private void Start()
	{
		handlers = FindObjectsOfType<ACommandHandler>();
	}

	public void Toggle()
	{
		foreach(ACommandHandler handler in handlers)
		{
			handler.ToggleImmediateExecution();
		}

		isExecuteImmediately = !isExecuteImmediately;
		
		buttonText.text = isExecuteImmediately ?
			"Build Queue" :
			"Execution";
	}
}

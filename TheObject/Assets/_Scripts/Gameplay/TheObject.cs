﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheObject : MonoBehaviour
{
	private Vector3 startingPosition;
	private Quaternion startingRotation;
	private Color startingColor;

	private Renderer renderer;

	private Coroutine movementRoutineRef;
	private Coroutine rotationRoutineRef;
	private Coroutine colorRoutineRef;

	[SerializeField]
	private float returnDuration;
	[SerializeField]
	private float returnDelay;
	[SerializeField]
	private AnimationCurve ease;

	private void Start()
	{
		renderer = GetComponent<Renderer>();

		startingPosition = transform.position;
		startingRotation = transform.rotation;

		startingColor = renderer.material.color;
	}

	public void ReturnToDefaultOrientation()
	{
		rotationRoutineRef = StartCoroutine(returnToDefaultOrientationRoutine());
	}

	public void RotationHandlerNotification()
	{
		if (rotationRoutineRef != null)
		{
			StopCoroutine(rotationRoutineRef);
			rotationRoutineRef = null;
		}
	}

	public void ReturnToOrigin()
	{
		movementRoutineRef = StartCoroutine(returnToOriginRoutine());
	}

	public void MovementHandlerNotification()
	{
		if (movementRoutineRef != null)
		{
			StopCoroutine(movementRoutineRef);
			movementRoutineRef = null;
		}
	}

	public void ReturnToOriginalColor()
	{
		colorRoutineRef = StartCoroutine(returnToOriginalColorRoutine());
	}

	public void ColorHandlerNotification()
	{
		if (colorRoutineRef != null)
		{
			StopCoroutine(colorRoutineRef);
			colorRoutineRef = null;
		}
	}

	private IEnumerator returnToOriginRoutine()
	{
		Vector3 startingPosition = transform.position;
		Vector3 finalPosition = this.startingPosition;

		float timeRemaining = returnDuration;

		yield return new WaitForSeconds(returnDelay);

		while(timeRemaining > 0)
		{
			transform.position = Vector3.Lerp(finalPosition, startingPosition, ease.Evaluate(timeRemaining / returnDuration));
			timeRemaining -= Time.deltaTime;
			yield return null;
		}

		transform.position = finalPosition;
		movementRoutineRef = null;
	}

	private IEnumerator returnToDefaultOrientationRoutine()
	{
		Quaternion startingRotation = transform.rotation;
		Quaternion finalRotation = this.startingRotation;

		float timeRemaining = returnDuration;

		yield return new WaitForSeconds(returnDelay);

		while (timeRemaining > 0)
		{
			transform.rotation = Quaternion.Slerp(finalRotation, startingRotation, ease.Evaluate(timeRemaining / returnDuration));
			timeRemaining -= Time.deltaTime;
			yield return null;
		}

		transform.rotation = finalRotation;
		rotationRoutineRef = null;
	}

	private IEnumerator returnToOriginalColorRoutine()
	{
		Color startColor = renderer.material.color;
		Color finalColor = startingColor;

		float timeRemaining = returnDuration;
		while (timeRemaining > 0)
		{
			renderer.material.color = Color.Lerp(finalColor, startColor,
				ease.Evaluate(timeRemaining / returnDuration));
			timeRemaining -= Time.deltaTime;
			yield return null;
		}

		renderer.material.color = finalColor;
		colorRoutineRef = null;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CommandListenerLegend : MonoBehaviour
{
	[SerializeField]
	private CommandListener commandListener;
	[SerializeField]
	private TextMeshProUGUI textPrefab;

	// Start is called before the first frame update
	void Start()
	{
		foreach(KeyValuePair<KeyCode, ACommand> keyCommandPair in commandListener.Commands)
		{
			TextMeshProUGUI legendEntry = Instantiate(textPrefab.gameObject, transform).GetComponent<TextMeshProUGUI>();
			legendEntry.text = $"{keyCommandPair.Key.ToString()} - {keyCommandPair.Value.name}";
		}
	}
}

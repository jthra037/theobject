﻿using System.Collections.Generic;
using UnityEngine;

public abstract class ACommandHandler : MonoBehaviour
{
	private bool pauseImmediateExecution;

	public Queue<ACommand> CommandQueue
	{
		get;
		private set;
	}

	virtual protected void Start()
	{
		CommandQueue = new Queue<ACommand>();
	}

	public void CommitCommand(ACommand command)
	{
		CommandQueue.Enqueue(command);
		if (!pauseImmediateExecution)
		{
			handleCommands();
		}
	}

	public void ToggleImmediateExecution()
	{
		if (pauseImmediateExecution)
		{
			handleCommands();
		}
		pauseImmediateExecution = !pauseImmediateExecution;
	}

	virtual public void ClearCommands()
	{
		CommandQueue.Clear();
	}

	abstract protected void performCommand(ACommand command);
	abstract protected void handleCommands();
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneShotAudioHandler : ACommandHandler, IAudioChanger
{
	private Coroutine commandRoutineRef;
	private AudioSource audioSource;

	public bool IsPlaying => audioSource.isPlaying;

	protected override void Start()
	{
		base.Start();
		audioSource = GetComponent<AudioSource>();
	}

	public void ChangeAudioClip(AudioClip clip)
	{
		commandRoutineRef = StartCoroutine(commandRoutine(clip));
	}

	protected override void handleCommands()
	{
		if (CommandQueue.Count > 0 && commandRoutineRef == null)
		{
			performCommand(CommandQueue.Dequeue());
		}
	}

	protected override void performCommand(ACommand command)
	{
		command.Execute(this);
	}

	private IEnumerator commandRoutine(AudioClip clip)
	{
		audioSource.PlayOneShot(clip);

		yield return new WaitWhile(() => audioSource.isPlaying);

		commandRoutineRef = null;
		handleCommands();
	}

	public void TogglePlaying()
	{
		if (IsPlaying)
		{
			audioSource.Pause();
		}
		else
		{
			audioSource.UnPause();
		}
	}
}

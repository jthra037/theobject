﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionHandler : ACommandHandler, IActionable
{
	[SerializeField]
	private float commandDuration;
	[SerializeField]
	private AnimationCurve ease;

	[SerializeField]
	private TheObject theObject;

	private Coroutine commandRoutineRef;

	public void Action(Vector3 movement, Vector3 eulerAngles)
	{
		commandRoutineRef = StartCoroutine(commandRoutine(movement, Quaternion.Euler(eulerAngles)));
		theObject.MovementHandlerNotification();
	}

	protected override void handleCommands()
	{
		if (CommandQueue.Count > 0 && commandRoutineRef == null)
		{
			performCommand(CommandQueue.Dequeue());
		}
		else if (CommandQueue.Count == 0)
		{
			theObject.ReturnToOrigin();
		}
	}

	protected override void performCommand(ACommand command)
	{
		command.Execute(this);
	}

	private IEnumerator commandRoutine(Vector3 movement, Quaternion rotation)
	{
		Vector3 startPosition = transform.position;
		Vector3 finalPosition = transform.position + movement;
		
		Quaternion startRotation = transform.rotation;
		Quaternion finalRotation = rotation * transform.rotation;

		float timeRemaining = commandDuration;
		while (timeRemaining > 0)
		{
			transform.position = Vector3.Lerp(finalPosition, startPosition,
				ease.Evaluate(timeRemaining / commandDuration));
			
			transform.rotation = Quaternion.Slerp(finalRotation, startRotation,
				ease.Evaluate(timeRemaining / commandDuration));
			
			timeRemaining -= Time.deltaTime;
			yield return null;
		}

		transform.position = finalPosition;
		transform.rotation = finalRotation;
		commandRoutineRef = null;
		handleCommands();
	}
}

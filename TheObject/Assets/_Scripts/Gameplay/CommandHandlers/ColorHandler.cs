﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorHandler : ACommandHandler, IColorable
{
	[SerializeField]
	private float commandDuration;
	[SerializeField]
	private AnimationCurve ease;

	[SerializeField]
	private TheObject theObject;

	private Coroutine commandRoutineRef;


	private Color originalColor;
	private Renderer renderer;

	override protected void Start()
	{
		base.Start();
		renderer = GetComponent<Renderer>();
		originalColor = renderer.material.color;
	}

	public void ChangeColor(Color color)
	{
		commandRoutineRef = StartCoroutine(commandRoutine(color));
		theObject.ColorHandlerNotification();
	}

	public void RunPattern(Color[] colors, float duration)
	{
		commandRoutineRef = StartCoroutine(patternRoutine(colors, duration));
		theObject.ColorHandlerNotification();
	}

	public void ReturnToOriginal()
	{
		commandRoutineRef = StartCoroutine(commandRoutine(originalColor));
		theObject.ColorHandlerNotification();
	}

	protected override void handleCommands()
	{
		if (CommandQueue.Count > 0 && commandRoutineRef == null)
		{
			performCommand(CommandQueue.Dequeue());
		}
	}

	protected override void performCommand(ACommand command)
	{
		command.Execute(this);
	}

	private IEnumerator commandRoutine(Color color)
	{
		Color startColor = renderer.material.color;
		Color finalColor = color;

		float timeRemaining = commandDuration;
		while (timeRemaining > 0)
		{
			renderer.material.color = Color.Lerp(finalColor, startColor,
				ease.Evaluate(timeRemaining / commandDuration));
			timeRemaining -= Time.deltaTime;
			yield return null;
		}

		renderer.material.color = finalColor;
		commandRoutineRef = null;
		handleCommands();
	}

	private IEnumerator patternRoutine(Color[] colors, float duration)
	{
		Color startColor = renderer.material.color;

		float singleColorDuration = duration / colors.Length;
		float timeRemaining = singleColorDuration;
		// ease into array
		while (timeRemaining > 0)
		{
			renderer.material.color = Color.Lerp(colors[0], startColor,
				ease.Evaluate(timeRemaining / singleColorDuration));
			timeRemaining -= Time.deltaTime;
			yield return null;
		}

		//ease through array
		for (int i = 0; i < colors.Length - 1; i++)
		{
			timeRemaining = singleColorDuration;
			while (timeRemaining > 0)
			{
				renderer.material.color = Color.Lerp(colors[i+1], colors[i],
					ease.Evaluate(timeRemaining / singleColorDuration));
				timeRemaining -= Time.deltaTime;
				yield return null;
			}

		}

		renderer.material.color = colors[colors.Length - 1];
		commandRoutineRef = null;
		handleCommands();
	}
}

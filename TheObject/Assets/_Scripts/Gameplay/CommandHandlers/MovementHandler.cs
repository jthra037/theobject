﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementHandler : ACommandHandler, IMoveable
{
	[SerializeField]
	private float commandDuration;
	[SerializeField]
	private AnimationCurve ease;

	[SerializeField]
	private TheObject theObject;

	private Coroutine commandRoutineRef;

	public void Move(Vector3 movement)
	{
		commandRoutineRef = StartCoroutine(commandRoutine(movement));
		theObject.MovementHandlerNotification();
	}

	protected override void handleCommands()
	{
		if (CommandQueue.Count > 0 && commandRoutineRef == null)
		{
			performCommand(CommandQueue.Dequeue());
		}
		else if (CommandQueue.Count == 0)
		{
			theObject.ReturnToOrigin();
		}
	}

	protected override void performCommand(ACommand command)
	{
		command.Execute(this);
	}

	private IEnumerator commandRoutine(Vector3 movement)
	{
		Vector3 startPosition = transform.position;
		Vector3 finalPosition = transform.position + movement;

		float timeRemaining = commandDuration;
		while(timeRemaining > 0)
		{
			transform.position = Vector3.Lerp(finalPosition, startPosition,
				ease.Evaluate(timeRemaining / commandDuration));
			timeRemaining -= Time.deltaTime;
			yield return null;
		}

		transform.position = finalPosition;
		commandRoutineRef = null;
		handleCommands();
	}
}

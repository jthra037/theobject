﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationHandler : ACommandHandler, IRotateable
{
	[SerializeField]
	private float commandDuration;
	[SerializeField]
	private AnimationCurve ease;

	[SerializeField]
	private TheObject theObject;

	private Coroutine commandRoutineRef;

	public void Rotate(Vector3 eulerAngles)
	{
		Rotate(Quaternion.Euler(eulerAngles.x, eulerAngles.y, eulerAngles.z));
	}

	public void Rotate(Quaternion rotation)
	{
		commandRoutineRef = StartCoroutine(commandRoutine(Quaternion.identity * rotation));
		theObject.RotationHandlerNotification();
	}

	protected override void handleCommands()
	{
		if (CommandQueue.Count > 0 && commandRoutineRef == null)
		{
			performCommand(CommandQueue.Dequeue());
		}
		else if (CommandQueue.Count == 0)
		{
			theObject.ReturnToDefaultOrientation();
		}
	}

	protected override void performCommand(ACommand command)
	{
		command.Execute(this);
	}

	private IEnumerator commandRoutine(Quaternion rotation)
	{
		Quaternion startRotation = transform.rotation;
		Quaternion finalRotation = rotation * transform.rotation;

		float timeRemaining = commandDuration;
		while (timeRemaining > 0)
		{
			transform.rotation = Quaternion.Slerp(finalRotation, startRotation,
				ease.Evaluate(timeRemaining / commandDuration));
			timeRemaining -= Time.deltaTime;
			yield return null;
		}

		transform.rotation = finalRotation;
		commandRoutineRef = null;
		handleCommands();
	}
}

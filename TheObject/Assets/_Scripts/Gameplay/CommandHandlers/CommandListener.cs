﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandListener : MonoBehaviour
{
	[SerializeField]
	private ACommandHandler commandHandler;

	[SerializeField]
	private CommandList commandList;

	public Dictionary<KeyCode, ACommand> Commands
	{
		get;
		private set;
	}

	private void Awake()
	{
		// better to only iterate and compile command map once
		Commands = commandList.GetCommandMap();
	}

	private void Update()
	{
		foreach(KeyValuePair<KeyCode, ACommand> command in Commands)
		{
			if (Input.GetKeyDown(command.Key))
			{
				commandHandler.CommitCommand(command.Value);
			}
		}
	}
}

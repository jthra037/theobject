﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHandler : ACommandHandler, IAudioChanger
{
	[SerializeField]
	private float commandDuration;
	[SerializeField]
	private AnimationCurve ease;

	private Coroutine commandRoutineRef;

	[SerializeField]
	private AudioSource primaryAudioSource;
	[SerializeField]
	private AudioSource secondaryAudioSource;

	private float startingVolume;

	private Dictionary<int, float> playBackTimes;

	public bool IsPlaying => primaryAudioSource.isPlaying;

	override protected void Start()
	{
		base.Start();
		secondaryAudioSource.Pause();
		playBackTimes = new Dictionary<int, float>();
		startingVolume = primaryAudioSource.volume;
	}

	public void ChangeAudioClip(AudioClip clip)
	{
		commandRoutineRef = StartCoroutine(commandRoutine(clip));
	}

	protected override void handleCommands()
	{
		if (CommandQueue.Count > 0 && commandRoutineRef == null)
		{
			performCommand(CommandQueue.Dequeue());
		}
	}

	protected override void performCommand(ACommand command)
	{
		command.Execute(this);
	}

	private void switchSources()
	{
		AudioSource temp = primaryAudioSource;
		primaryAudioSource = secondaryAudioSource;
		secondaryAudioSource = temp;
	}

	private IEnumerator commandRoutine(AudioClip clip)
	{
		float playBackTime;
		playBackTimes.TryGetValue(clip.GetInstanceID(), out playBackTime);

		secondaryAudioSource.clip = clip;
		secondaryAudioSource.time = playBackTime;
		secondaryAudioSource.Play();

		float timeRemaining = commandDuration;

		while (timeRemaining > 0)
		{
			primaryAudioSource.volume = ease.Evaluate(timeRemaining / commandDuration) * startingVolume;
			secondaryAudioSource.volume = startingVolume - primaryAudioSource.volume;
			timeRemaining -= Time.deltaTime;
			yield return null;
		}

		primaryAudioSource.volume = 0;
		secondaryAudioSource.volume = startingVolume;

		primaryAudioSource.Pause();
		playBackTimes[primaryAudioSource.clip.GetInstanceID()] = primaryAudioSource.time;
		switchSources();

		commandRoutineRef = null;
		handleCommands();
	}

	public void TogglePlaying()
	{
		if (IsPlaying)
		{
			primaryAudioSource.Pause();
		}
		else
		{
			primaryAudioSource.UnPause();
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowRotate : MonoBehaviour
{
	[SerializeField]
	private Vector3 eulerAngles;
	private Quaternion rotation;

	private Quaternion startingRotation;

	[SerializeField]
	private float rotationSpeed;

	private bool running = true;

	private void Start()
	{
		rotation = Quaternion.Euler(eulerAngles);
		startingRotation = transform.rotation;
	}

	// Update is called once per frame
	void Update()
	{
		if (running)
		{
			transform.rotation = Quaternion.Slerp(transform.rotation, transform.rotation * rotation,
				Time.deltaTime * rotationSpeed);
		}
	}

	public void ToggleRunning()
	{
		if (running)
		{
			transform.rotation = startingRotation;
		}
		running = !running;
	}
}

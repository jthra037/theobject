﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Text;

public class ShowQueue : MonoBehaviour
{
	private TextMeshProUGUI queue;

	[SerializeField]
	private ACommandHandler commandHandler;
	StringBuilder builder;

	private void Start()
	{
		queue = GetComponent<TextMeshProUGUI>();
		builder = new StringBuilder();
	}

	private void Update()
	{
		foreach(ACommand command in commandHandler.CommandQueue)
		{
			builder.AppendLine(command.name);
		}

		queue.text = builder.ToString();
		builder.Clear();
	}
}
